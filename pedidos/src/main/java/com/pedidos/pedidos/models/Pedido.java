package com.pedidos.pedidos.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
public class Pedido implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String nome;
	private String endereco;
	private String tipo_pao;
	private String tipo_salada;
	private double valor;
	private String data;
	private String molho;
	private String tempero;
	private String tipo_pagamento;
	private Boolean troco;
	private String tipo_lanche;
	
	
	///////getters and setters ////////
	
	public String getTipo_lanche() {
		return tipo_lanche;
	}
	public void setTipo_lanche(String tipo_lanche) {
		this.tipo_lanche = tipo_lanche;
	}
	public Boolean getTroco() {
		return troco;
	}
	public void setTroco(Boolean troco) {
		this.troco = troco;
	}
	public String getTipo_pagamento() {
		return tipo_pagamento;
	}
	public void setTipo_pagamento(String tipo_pagamento) {
		this.tipo_pagamento = tipo_pagamento;
	}
	public String getMolho() {
		return molho;
	}
	public void setMolho(String molho) {
		this.molho = molho;
	}
	public String getTempero() {
		return tempero;
	}
	public void setTempero(String tempero) {
		this.tempero = tempero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTipo_pao() {
		return tipo_pao;
	}
	public void setTipo_pao(String tipo_pao) {
		this.tipo_pao = tipo_pao;
	}
	public String getTipo_salada() {
		return tipo_salada;
	}
	public void setTipo_salada(String tipo_salada) {
		this.tipo_salada = tipo_salada;
	}
	public double getValor() {
		return valor;
	}
	public void setValor(double valor) {
		this.valor = valor;
	}
	public String getData() {
		return data;
	}
	public void setData(String data) {
		this.data = data;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

}
