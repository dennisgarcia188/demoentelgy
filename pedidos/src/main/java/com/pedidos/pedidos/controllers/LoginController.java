package com.pedidos.pedidos.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pedidos.pedidos.models.Cliente;
import com.pedidos.pedidos.repository.ClienteRepository;

@Controller
public class LoginController {

	@Autowired
	private ClienteRepository clienteRepository;

	@RequestMapping(value = "/efetuarLogin", method = RequestMethod.GET)
	public String form() {
		return "pedidos/formLogin";
	}

	@RequestMapping(value = "/efetuarLogin", method = RequestMethod.POST)
	public String formRedirect() {
		return "redirect:/cadastrarPedido";
	}

	@RequestMapping("/{id}")
	public ModelAndView nomeCliente(@PathVariable("id") Integer id) {
		Cliente cliente = clienteRepository.findById(id);
		ModelAndView modelAndView = new ModelAndView("formPedido");
		modelAndView.addObject("cliente", cliente);
		return modelAndView;
	}
}
