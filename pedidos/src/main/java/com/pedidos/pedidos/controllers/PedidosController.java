package com.pedidos.pedidos.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pedidos.pedidos.models.Pedido;
import com.pedidos.pedidos.models.Produto;
import com.pedidos.pedidos.repository.PedidosRepository;
import com.pedidos.pedidos.repository.ProdutoRepository;

@Controller
public class PedidosController {
	
	@Autowired
	private PedidosRepository pedidosRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@RequestMapping(value="/cadastrarPedido", method=RequestMethod.GET)
	public String form() {
		return "pedidos/formPedido";
	}
	
	@RequestMapping(value="/cadastrarPedido", method=RequestMethod.POST)
	public String form(Pedido pedido) {
		pedidosRepository.save(pedido);
		return "redirect:/pedidoFinalizado";
	}
	
	@RequestMapping("/cadastrarPedido")
	public ModelAndView listaProdutos() {
		ModelAndView modelAndView = new ModelAndView("formPedido");
		Iterable<Produto> produtos = produtoRepository.findAll();
		modelAndView.addObject("produtos", produtos);
		return modelAndView;
	}

}
