package com.pedidos.pedidos.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.pedidos.pedidos.models.Usuario;
import com.pedidos.pedidos.repository.UsuarioRepository;

@Controller
public class ClienteController {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@RequestMapping(value="/cadastrarCliente", method=RequestMethod.GET)
	public String form() {
		return "pedidos/formCliente";
	}
	
	@RequestMapping(value="/cadastrarCliente", method=RequestMethod.POST)
	public String form(@Valid Usuario usuario, BindingResult result, RedirectAttributes attributes) {
		if(result.hasErrors()) {
			attributes.addAttribute("mensagem", "This fields is required");
			return "redirect:/cadastrarCliente";
		}
		usuario.setSenha((new BCryptPasswordEncoder().encode(usuario.getSenha())));
		usuarioRepository.save(usuario);
		attributes.addAttribute("mensagem", "Cliente cadastrado com sucesso!");
		return "redirect:/efetuarLogin";
	}

}
