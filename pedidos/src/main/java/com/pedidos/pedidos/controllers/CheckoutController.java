package com.pedidos.pedidos.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pedidos.pedidos.models.Pedido;

@Controller
public class CheckoutController {
	
	@RequestMapping("/finalizarPedido")
	public String form() {
		return "pedidos/formCheckout";
	}
	
	@RequestMapping(value="/finalizarPedido", method=RequestMethod.POST)
	public String form(Pedido pedido) {
		return "redirect:/pedidoFinalizado";
	}

}
