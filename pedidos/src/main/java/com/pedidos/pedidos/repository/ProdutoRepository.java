package com.pedidos.pedidos.repository;

import org.springframework.data.repository.CrudRepository;

import com.pedidos.pedidos.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, String>{

	
}
