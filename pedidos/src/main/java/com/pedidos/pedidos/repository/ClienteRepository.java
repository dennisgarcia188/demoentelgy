package com.pedidos.pedidos.repository;

import org.springframework.data.repository.CrudRepository;

import com.pedidos.pedidos.models.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, String>{

	Cliente findById(Integer id);
	
}
