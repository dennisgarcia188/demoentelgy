package com.pedidos.pedidos.repository;

import org.springframework.data.repository.CrudRepository;

import com.pedidos.pedidos.models.Pedido;

public interface PedidosRepository extends CrudRepository<Pedido, String> {

}
