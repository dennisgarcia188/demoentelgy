package com.pedidos.pedidos.repository;

import org.springframework.data.repository.CrudRepository;

import com.pedidos.pedidos.models.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, String>{

	Usuario findByLogin(String login);
	
}
